#!/bin/bash

#######################################
##
##  Generateur de Docker Compose
##
#######################################


DIR="${HOME/generator}" #Localisation de nos Datas
USER_SCRIPT=${USER} #compte qui run le script




#fonction help qui affiche les options possibles
# ${0} = le premier élément saisi dans le prompt, normalement "./generator.sh"
# ${0##*/} = vire le ./ "./generator.sh" deviendra "generator.sh" pour que ça soit plus joli
help(){
echo " USAGE :

    ${0##*/} [-h] [--help]

    Options:

    -h, --help : aides

    -p, --postgres : lance une instance postgres

    -i, --ip : affichage des ip

    "
}

ip() {
for i in $(docker ps -q); do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;done
}



postgres(){

echo ""
echo "Installation d'une instance postgres..."
echo ""
echo "1 - Création du répertoire de datas.."
mkdir -p $DIR/postgres
echo ""
echo "
version: '3.7'
services:
  postgres:
    image: postgres:latest
    container_name: postgres
    environment:
    - POSTGRES_USER=myuser
    - POSTGRES_PASSWORD=password
    - POSTGRES_DB=mydb
    ports:
    - 5432:5432
    volumes:
    - postgres_data:/var/lib/postgres
    networks:
    - generator
volumes:
  postgres_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/postgres
networks:
  generator:
    driver: bridge
    ipam:
      config:
        - subnet: 192.169.0.0/24


" >$DIR/docker-compose-postgres.yml

echo "2 - Run de l'instance..."
docker-compose -f $DIR/docker-compose-postgres.yml  up -d

echo ""
echo "
Credentials :
    - PORT : 5432
    - POSTGRES_USER: myuser
    - POSTGRES_PASSWORD: password
    - POSTGRES_DATABASE: mydb

Command : psql -h <ip> -u myuser -d mydb
"
}

ip() {
for i in $(docker ps -q); do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;done
}

clean(){
NAME_CONTENEUR=$1
[ -z ${NAME_CONTENEUR} ] && exit 1
docker-compose -f $DIR/docker-compose-${NAME_CONTENEUR}.yml down
#[ ! -z ${NAME_CONTENEUR} ] && rm -rf $DIR/${NAME_CONTENEUR}
rm -f $DIR/docker-compose-postgres${ID_CONTENEUR}.yml
docker volume prune -f
}

# fonction qui va récupérer les options saisies par l'utilisateur
parser_options(){
# $@ = tout ce qui est saisi comme options
case $@ in 
    -h|--help) #h ou help comme option
        help #fonction à lancer pour ce cas là
    ;;
    -p|--postgres)
        postgres
    ;;
    -i|--ip)
        ip
    ;;
    *)
    echo "option invalide, lancez -h ou --help" # tous les autres cas
esac #fermeture du case
}

parser_options $@ #finalement on exécute le parser qui se charge d’exécuter la bonne fonction par rapport à l’option




